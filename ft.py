# for python3
import functools as _ft
import itertools

from functools import partial,reduce

from itertools import starmap

import operator as op
from operator import methodcaller as mcall

import numpy as np




def identity(arg):
    '''returns its unchanged argument'''
    return arg

def caller(*a, **kw):
    '''returns f: f(g) -> g(*a, **kw)'''
    return lambda g: g(*a, **kw)

def slicer(f, *a, **kw):
    '''(slice) || (slice-args) -> f(seq):seq[slice]'''
    slc = f if type(f) == slice else slice(f, *a, **kw)
    return lambda seq: seq[slc]

def ite(condition, result1, result2=None):
    '''if-then-else as a function'''
    return result1 if condition else result2

def applyif(condition, function, value):
    '''applies function to value if condition(value) is true, otherwise returns value unchanged'''
    return function(value) if condition else value

def fite(pred, action, inaction=identity):
    '''functional if-then-else: returns function of v that applies action if pred(v) is true, inaction otherwise'''
    return lambda v: action(v) if pred(v) else inaction(v)

#def fcond(ppairs, default=None):
#    pass


def starify(fun):
    '''wraps fun of n args, returns same fun that takes an iterable of the args)'''
    return lambda posargs: fun(*posargs)

def starifykw(fun):
    '''like starify, but new function takes a dict of kwargs'''
    return lambda dictargs: fun(**dictargs)

def starifyall(fun):
    '''as starify, but does both:
    fun(*args, **kwargs) -> f2(args, kwargs):fun(*args, **kwargs)'''
    return lambda posargs, dictargs: fun(*posargs, **dictargs)

#function composition

#TODO: do this properly
def comp2(f1, f2):
    '''composition of two functions: comp(f1, f2)(x) = f1(f2(x))'''
    composition = lambda *args, **kwargs: f1(f2(*args, **kwargs))
    composition.__name__ = "comp2({}, {})".format(f1.__name__, f2.__name__)
    composition.f1 = f1
    composition.f2 = f2
    return composition

def compose(f1, *funs):
    '''composition of an arbitrary amount of functions'''
    if len(funs) > 0:
        composition = comp2(f1, compose(*funs))
        composition.__str__ = lambda: "compose({}, {})".format(f1.__name__, ', '.join((f.__name__ for f in funs)))
        return composition
    else:
        return f1

comp = compose

#maps and other transducers

mapl = comp(list,map)
filterl = comp(list,filter)

def mapnpa(*args, **kwargs):
    '''like map, but returns result as np.array'''
    return np.array(mapl(*args, **kwargs))

def pmap(f):
    '''map transducer:
    pmap(f) -> g(*s) = map(f, *s)'''
    return partial(map, f)


def jmap(arg, funs):
    '''"inverse" (juxtaposed) map: applies each fun in funs to the same set of args'''
    return juxt_partial_map(*funs)(arg)

def juxt_partial_map(*funs):
    '''returns a function that applies the juxtaposition of all funs to its args:
    g(*args) = (fun(*args) for fun in funs)'''
    return lambda *args: (fun(*args) for fun in funs)

juxt = lambda *funs: comp(tuple, juxtpmap(funs))

def mapvals(f, dic):
    '''returns dict with each value replaced by the output of f on it'''
    return {k:f(v) for k,v in dic.items()}


def getargs(*args, **kwargs):
    return args, kwargs

indexgetter = lambda n: lambda obj: obj[n]
getindex = indexgetter
first = indexgetter(0)
ifirst = lambda s: next(itertools.islice(s,1))
second = indexgetter(1)
isecond = lambda s: next(itertools.islice(s,1,2))
rest = slicer(1,None)
last = indexgetter(-1)


def dunion(*dicts):
    '''creates a new dict which is the union of all the arguments'''
    return dict(itertools.chain(*(d.items() for d in dicts)))
