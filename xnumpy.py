from numpy import *
import numpy.linalg as npla

def normalize(vec, zerothresh = .00001):
    l = npla.norm(vec)
    return vec/l if l>zerothresh else None

def mplot(*args):
    [plt.plot(a) for a in args]
